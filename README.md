## Description

GUI tool to align a series of images with detectable contours. This includes rotation, translation and very small amounts of scaling (to account for organic deformations). Optimized to align a series of ellipsoid images like eyes.

#### Dependencies

Except for Python itself, all dependencies are fetched automatically.

- Python3
- opencv-python
- numpy
- matplotlib
- scikit-image
- argparse

## Installation

1. Open CLI
    - [Windows](https://www.dell.com/support/kbdoc/de-de/000130703/the-command-prompt-what-it-is-and-how-to-use-it-on-a-dell-system?lang=en): Press `[Windows-Key]` + `[R]`, then type "cmd.exe" and execute
    - [MacOS](https://support.apple.com/de-de/guide/terminal/apd5265185d-f365-44cb-8b09-71a064a42125/mac): Open Launchpad and type "Terminal" into search field, then click on the found program
    - [Linux](https://wiki.ubuntuusers.de/Terminal/): Depends on distribution, but Ubuntu has the shortcut `[Ctrl]` + `[Alt]` + `[T]`
2. Navigate to source folder
    - Type in CLI: `cd path/to/folder`
    - E.g. in Windows: `cd C:\User\MyName\Downloads\eye-align`
    - E.g. in MacOS: `cd ~/Downloads/eye-align`
    - E.g. in Linux: `cd ~/Downloads/eye-align`
3. Install with pip (Python): `pip install .`
4. Finished. Now you can use the program anywhere with the syntax shown in [Usage](#usage).

## Usage

`python -m eyealign Dataset-Path [Options]`

| Option | Effect |
| ------ | ------ |
| `--help` | Display help |
| `--out <Out-Path>` |  Path to overwrite the default output location (i.e. `dataset/processed/`). Careful, the specified path will be populated with all saved images. |
| `--max-scaling <value>` |Allowed (bidirectional) scaling factor during alignment. 0.0 means no scaling, 0.5 means scaling in range `[0.5, 1.5]`. Default is 0.08. |
| `--discard-score <value>` |  Images whose alignment score is below this threshold are discarded automatically. Choose value in range `[0.0, 1.0]`. Default is 0.3. |
| `--placeholder` | If set will save black placeholder images (replicas of valid images) for all deleted images. By default deleted images will not be saved in any way. |

## Post-Processing in 3D Slicer

- Load an image sequence: [Docs](https://slicer.readthedocs.io/en/latest/user_guide/modules/volumes.html#load-a-series-of-png-jpeg-or-tiff-images-as-volume)
- Change in the [Volumes Module](https://slicer.readthedocs.io/en/latest/user_guide/modules/volumes.html) the slice thickness
- Go to the [Segment Editor Module](https://slicer.readthedocs.io/en/latest/user_guide/modules/segmenteditor.html) and:
  - Add a new segment and brush initial seeds (you need at least two, i.e., one for "Air"/Nothing, one for the actual eye)
  - Then let the seeds grow and check the result
  - Click on "Show 3D" to see a preliminary 3D reconstruction
  - Once satisfied, hit "Apply"

from setuptools import setup

setup(name='eyealign',
      version='1.0',
      description='Small GUI tool to align a series of clearly contoured images. This includes rotation, translation and very small amounts of scaling.',
      url='https://git-ce.rwth-aachen.de/frieder.milke/eye-align',
      author='Jan Frieder Milke',
      author_email='milke@vis.rwth-aachen.de',
      license='MIT',
      packages=['eyealign'],
      install_requires=[
          'opencv-python',
          'numpy',
          'matplotlib',
          'scikit-image',
          'argparse',
      ],
      zip_safe=False)
import copy
from json.encoder import INFINITY
import time
import os
import cv2 as cv
import numpy as np
import argparse
from numpy.lib.stride_tricks import as_strided
import matplotlib.pyplot as plt
from skimage.metrics import structural_similarity as ssim
import threading
from pathlib import Path
from tkinter.filedialog import askdirectory

# Create the parser
parser = argparse.ArgumentParser(
                    prog='EyeAlign',
                    description='GUI tool to align a series of images with detectable contours. This includes rotation, translation and very small amounts of scaling (to account for organic deformations). Optimized to align a series of ellipsoid images like eyes.',
                    usage="python -m eyealign 'D:\\My Dataset'",
                    epilog= "For best results, preprocess images to have more clearly visible contours (e.g. with ImageJ)."
                            "In the tool, delete images which are too heavily deformed or otherwise degenerated."
                            "Then look for a suitable image to start the aligning process somewhere in the middle of the stack and invocate the alignment process."
                            "Be sure to select the correct mode before you click on save (only the currently selected mode will be saved)."
                    )
# Add an argument
parser.add_argument('data', nargs='?', type=str, help="Path to the folder containing a series of images to be aligned. Only works if images belong to a single dataset.")
parser.add_argument('--out', type=str, required=False, help="Path to overwrite the default output location (i.e. dataset/aligned/). Careful, the specified path will be populated with all saved images.")
parser.add_argument('--max-scaling', type=float, required=False, help="Allowed (bidirectional) scaling factor during alignment. 0.0 means no scaling, 0.5 means scaling in range [0.5, 1.5]. Default is 0.08.", default=0.08)
parser.add_argument('--discard-score', type=float, required=False, help="Images whose alignment score is below this threshold are discarded automatically. Choose value in range [0.0, 1.0]. Default is 0.3.", default=0.3)
parser.add_argument('--placeholder', action='store_true', help="If set will save black placeholder images (replicas of valid images) for all deleted images. By default deleted images will not be saved in any way.")

# Parse the argument
args = parser.parse_args()
if args.data is not None:
    data_location = args.data
else:
    data_location = askdirectory()
out_location = data_location + os.path.sep + "processed"
if args.out is not None:
    out_location = args.out
save_placeholders = args.placeholder
allowed_scale = args.max_scaling
quality_threshold = args.discard_score

# ----------------
# Global Variables
# ----------------

no_arguments = False

MAX_FEATURES = 500
GOOD_MATCH_PERCENT = 0.1
S_PER_UPDATE = 0.144

thread_asc = threading.Thread()
thread_a_prog = -1
thread_a_percent = 0.0
thread_desc = threading.Thread()
thread_d_prog = -1
thread_d_percent = 0.0


BACKGROUND_WHITE = True

################################################################

# ----------------
# Helper Functions
# ----------------

def strided_convolution(image, weight, stride):
    im_h, im_w = image.shape
    f_h, f_w = weight.shape

    out_shape = (1 + (im_h - f_h) // stride, 1 + (im_w - f_w) // stride, f_h, f_w)
    out_strides = (image.strides[0] * stride, image.strides[1] * stride, image.strides[0], image.strides[1])
    windows = as_strided(image, shape=out_shape, strides=out_strides)

    return np.tensordot(windows, weight, axes=((2, 3), (0, 1)))

def display_heatmap(heatmap):
    plt.imshow(heatmap, cmap='hot', interpolation='nearest', aspect='auto')
    plt.show()

def restrict(val, minval, maxval):
    if val < minval: return minval
    if val > maxval: return maxval
    return val

def crop_image(img, x_start, y_start, x_extent, y_extent):
    assert(len(img.shape) >= 2)

    # Crop image to fit the selected rectangle
    if (img.shape[0] >= y_start + y_extent) and (img.shape[1] >= x_start + x_extent) and (img.shape[0] >= 0) and (img.shape[1] >= 0):
        return img[y_start:y_start+y_extent, x_start:x_start+x_extent]
    else:
        return img

def get_extreme_anchors(anchor0, anchor1):
    if (anchor0 is None) or (anchor1 is None):
        return anchor0, anchor1

    anchor_min = (min(anchor0[0], anchor1[0]), min(anchor0[1], anchor1[1]))
    anchor_max = (max(anchor0[0], anchor1[0]), max(anchor0[1], anchor1[1]))

    return anchor_min, anchor_max


def pad_image(img, target_width, target_height, offset_x, offset_y):
    y_extent, x_extent = img.shape

    if (x_extent == target_width) and (y_extent == target_height):
        return img

    img_padded = cv.copyMakeBorder(img,
            top=offset_y,
            bottom=target_height - y_extent - offset_y,
            left=offset_x,
            right=target_width - x_extent - offset_x,
            borderType=cv.BORDER_CONSTANT,
            value=0)

    return img_padded

def fit_heatmap(heatmap, pixel_scale=1.0, target_width=None, target_height=None, offset_x=0, offset_y=0, apply_color=False):
    h_height, h_width = heatmap.shape

    if (target_width is None) or (target_height is None):
        target_height = h_height * pixel_scale
        target_width = h_width * pixel_scale

    heatmap = cv.resize(heatmap, (h_width * pixel_scale, h_height * pixel_scale), interpolation=cv.INTER_NEAREST_EXACT).astype(np.uint8)
    
    heatmap = pad_image(heatmap, target_width, target_height, offset_x, offset_y)

    if apply_color:
        heatmap = cv.applyColorMap(heatmap, cv.COLORMAP_HOT)

    return heatmap

def dilate_contours(img, kernel_size):
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (kernel_size, kernel_size))
    img_dilated = cv.dilate(img, kernel)
    return img_dilated

def gui_update(window, stateinfo):
    si = stateinfo
 
    if (si.mouse_lbutton) or (si.mouse_rbutton):
        si.draw_gui = False

    if (si.mouse_lbutton_drag):
        window_size = cv.getWindowImageRect(window.name)
        si.anchor1_x = restrict(si.mouse_x, 0, window_size[2])
        si.anchor1_y = restrict(si.mouse_y, 0, window_size[3])
        window.canvas = cv.rectangle(window.canvas, (si.anchor0_x, si.anchor0_y), (si.anchor1_x, si.anchor1_y), color=(0, 0, 255), thickness=2, lineType=cv.LINE_AA)
        si.draw_gui = True
    elif si.draw_gui:
        window.canvas = cv.rectangle(window.canvas, (si.anchor0_x, si.anchor0_y), (si.anchor1_x, si.anchor1_y), color=(0, 0, 255), thickness=2, lineType=cv.LINE_AA)

def gui_get_selected_area(stateinfo):
    """Returns two corners of the selected rectangle as (minx, miny), (maxx, maxy)"""
    si = stateinfo

    if si.draw_gui:
        anchor0 = (si.anchor0_x, si.anchor0_y)
        anchor1 = (si.anchor1_x, si.anchor1_y)
        anchor_min, anchor_max = get_extreme_anchors(anchor0, anchor1)

        # Don't return an empty area
        if (anchor_max[0] - anchor_min[0] > 0) and (anchor_max[1] - anchor_min[1] > 0):
            return anchor_min, anchor_max

    return None, None

def cv_draw_multiline_text(
    img,
    text,
    uv_top_left,
    color=(255, 255, 255),
    fontScale=0.5,
    thickness=1,
    fontFace=cv.FONT_HERSHEY_SIMPLEX,
    outline_color=(0, 0, 0),
    line_spacing=1.5,
    lineType=cv.LINE_AA
):
    assert isinstance(text, str)

    uv_top_left = np.array(uv_top_left, dtype=float)
    assert uv_top_left.shape == (2,)

    for line in text.splitlines():
        (w, h), _ = cv.getTextSize(
            text=line,
            fontFace=fontFace,
            fontScale=fontScale,
            thickness=thickness,
        )
        uv_bottom_left_i = uv_top_left + [0, h]
        org = tuple(uv_bottom_left_i.astype(int))

        if outline_color is not None:
            cv.putText(
                img,
                text=line,
                org=org,
                fontFace=fontFace,
                fontScale=fontScale,
                color=outline_color,
                thickness=thickness * 3,
                lineType=lineType,
            )
        cv.putText(
            img,
            text=line,
            org=org,
            fontFace=fontFace,
            fontScale=fontScale,
            color=color,
            thickness=thickness,
            lineType=lineType,
        )

        uv_top_left += [0, h * line_spacing]

def get_contour_center(contour):
    M = cv.moments(contour)
    if M['m00'] == 0:
        return (None, None)

    # Contour center
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])
    return (cx, cy)

################################################################

# ---------
# Callbacks
# ---------

def mouse_cb(event, x, y, flags, param):
    atlas = param[0]
    img_nr = param[1]
    stateinfo = param[2]

    if event == cv.EVENT_LBUTTONDOWN:
        stateinfo.anchor0_x = x
        stateinfo.anchor0_y = y

    if event == cv.EVENT_MOUSEMOVE:
        if stateinfo.mouse_lbutton:
            stateinfo.mouse_lbutton_drag = True
        elif stateinfo.mouse_rbutton:
            stateinfo.mouse_rbutton_drag = True
    
    if event == cv.EVENT_LBUTTONUP:
        stateinfo.mouse_lbutton_drag = False

    if event == cv.EVENT_RBUTTONUP:
        stateinfo.mouse_rbutton_drag = False

    stateinfo.mouse_x = x
    stateinfo.mouse_y = y
    stateinfo.mouse_lbutton = event == cv.EVENT_LBUTTONDOWN
    stateinfo.mouse_rbutton = event == cv.EVENT_RBUTTONDOWN

################################################################

# ---------------
# Image Alignment
# ---------------

def preprocess_image(atlas_image, bin_size=32, hm_threshold1=50, hm_threshold2=35, canny_threshold1=100, canny_threshold2=200, dilation_kernel=9, anchor0=None, anchor1=None):
    """Returns an image containing contours based on features."""
    # Input image
    img = atlas_image.img
    height, width = img.shape[:2]

    # If ROI: Crop image to fit the selected rectangle
    roi_ox = 0
    roi_oy = 0
    if (anchor0 is not None) and (anchor1 is not None):
        anchor_min, anchor_max = get_extreme_anchors(anchor0, anchor1)

        roi_ox = anchor_min[0]
        roi_oy = anchor_min[1]
        
        x_extent = anchor_max[0] - anchor_min[0]
        y_extent = anchor_max[1] - anchor_min[1]

        img = crop_image(img, roi_ox, roi_oy, x_extent, y_extent)
        
    # Find edge features and filter the results
    img_edges, heatmap = feature_noisefilter_heatmap_canny(img,
        bin_size=bin_size,
        filter_threshold_trigger=hm_threshold1,
        filter_threshold_invalid=hm_threshold2,
        canny_threshold1=canny_threshold1,
        canny_threshold2=canny_threshold2
    )
    # Fit heatmap to image
    heatmap = fit_heatmap(heatmap,
        pixel_scale=bin_size,
        target_width=width,
        target_height=height,
        offset_x=roi_ox,
        offset_y=roi_oy,
        apply_color=True
    )

    # Dilate (enlarge) the edges such that gaps in the shapes are closed
    img_edges = dilate_contours(img_edges, kernel_size=dilation_kernel)

    # Extract contour
    contours, best_id = extract_outer_contours(img_edges)
    img_contours = draw_contour((height, width), contours, best_id, (roi_ox, roi_oy))
    img_filtered = filter_by_contour(img, contours, best_id, (roi_ox, roi_oy))

    # Save
    #atlas_image.featuremap = img_edges
    #atlas_image.heatmap = heatmap
    #atlas_image.contourmap = img_contours
    atlas_image.filtered_img = img_filtered

    # Prealign
    center_by_contour(atlas_image, contours, best_id, (roi_ox, roi_oy))

def align_ECC(im1, im2, mode, restrict_scale = True):
    global allowed_scale
    
    im1_gray = im1.get_by_mode(mode)
    im2_gray = im2.get_by_mode(mode)
    if (len(im1_gray.shape) > 2):
        im1_gray = cv.cvtColor(im1.get_by_mode(mode), cv.COLOR_BGR2GRAY)
        im2_gray = cv.cvtColor(im2.get_by_mode(mode), cv.COLOR_BGR2GRAY)

    warp_mode = cv.MOTION_AFFINE

    # 2x3 matrix for affine transformation
    warp_matrix = np.eye(2, 3, dtype=np.float32)
    cc = 0.0

    # Termination criteria
    number_of_iterations = 500  # timeout
    termination_eps = 1e-7     # precision
    termination_criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, number_of_iterations, termination_eps)

    # Images mask (ignore borders which may go black due to transformation)
    try:
        (cc, warp_matrix) = cv.findTransformECC(im1_gray, im2_gray, warp_matrix, warp_mode, termination_criteria)
        im2.score = cc

        # Restrict scaling:
        # 1. Take left submatrix of T with 2x2 (encodes linear transform)
        # 2. Extract rotations u, v and scaling s via SVD
        # 3. Restrict scaling to specified range
        # 4. Compose transformations as u @ s @ v again
        target_min = 1.0 - allowed_scale
        target_max = 1.0 + allowed_scale

        u, s, v = np.linalg.svd(warp_matrix[:2, :2], full_matrices=True)
        im2.scale_x = np.round(s[0], 3)
        im2.scale_y = np.round(s[1], 3)
        scalings = np.asarray((im2.scale_x, im2.scale_y))
        
        # If scale is too strong, fiddle with the transformation matrix
        if restrict_scale and ((np.any(scalings < target_min)) or (np.any(scalings > target_max))):
            scaling_matrix = np.eye(2,2)
            scaling_matrix[0,0] = restrict(s[0], target_min, target_max)
            scaling_matrix[1,1] = restrict(s[1], target_min, target_max)
            #scaling_matrix[0,0] = s[0]
            #scaling_matrix[1,1] = s[1]

            # Recompose without translation
            warp_matrix[:2,:2] = u @ (scaling_matrix @ v)

            im2.scale_x = np.round(scaling_matrix[0,0], 3)
            im2.scale_y = np.round(scaling_matrix[1,1], 3)

            # Apply modified transformation
            im2.transform_affine(warp_matrix, flags=cv.INTER_LANCZOS4 + cv.WARP_INVERSE_MAP)

            # Get transformed image
            im2_gray = im2.get_by_mode(mode)
            if (len(im2_gray.shape) > 2):
                im2_gray = cv.cvtColor(im2.get_by_mode(mode), cv.COLOR_BGR2GRAY)

            # Reestimate translation (and rotation if necessary)
            warp_matrix = np.eye(2, 3, dtype=np.float32)
            cc, warp_matrix = cv.findTransformECC(im1_gray, im2_gray, warp_matrix, cv.MOTION_EUCLIDEAN , termination_criteria)
            im2.score = cc

            # Apply
            im2.transform_affine(warp_matrix, flags=cv.INTER_LANCZOS4 + cv.WARP_INVERSE_MAP)
        else:
            # Apply unmodified transformation
            im2.transform_affine(warp_matrix, flags=cv.INTER_LANCZOS4 + cv.WARP_INVERSE_MAP)
    except cv.error as e:
        print(e)
        im2.score = 0.0

def threaded_align_ECC(start_id, end_id, atlas, mode, score_thres = 0.7):
    global thread_a_prog
    global thread_d_prog
    global thread_a_percent
    global thread_d_percent
    
    id_template = start_id

    # Increment or decrement
    if start_id < end_id:
        id_compare = start_id + 1

        while ((id_compare <= end_id) and (id_template < end_id)):
            # Only select valid images (not marked for destruction)
            if atlas[id_compare].discard:
                id_compare += 1
                continue

            if atlas[id_template].discard:
                id_template += 1
                continue

            thread_a_prog = id_compare

            # Align
            align_ECC(atlas[id_template], atlas[id_compare], mode)
            thread_a_percent += 100.0/abs(end_id - start_id)
            
            # If bad, discard
            if atlas[id_compare].score < score_thres:
                atlas.mark_discard(id_compare)

                # Only increment id_compare
                id_compare += 1
                continue
            # Else, all is fine
            else:
                id_template += 1
                id_compare += 1
        
        thread_a_prog = -1
        thread_a_percent = 100.0
    else:
        id_compare = start_id - 1

        while ((id_compare >= end_id) and (id_template > end_id)):            
            # Only select valid images (not marked for destruction)
            if atlas[id_compare].discard:
                id_compare -= 1
                continue

            if atlas[id_template].discard:
                id_template -= 1
                continue

            thread_d_prog = id_compare

            # Align
            align_ECC(atlas[id_template], atlas[id_compare], mode)
            thread_d_percent += 100.0/abs(end_id - start_id)
            
            # If bad, discard
            if atlas[id_compare].score < score_thres:
                atlas.mark_discard(id_compare)

                # Only increment id_compare
                id_compare -= 1
                continue
            # Else, all is fine
            else:
                id_template -= 1
                id_compare -= 1

        thread_d_prog = -1
        thread_d_percent = 100.0

def center_by_contour(atlas_image, contours, contour_id, contour_offset=(0,0)):
    """Transforms atlas image to be centered by a contour centerpoint.

    If contours were gathered in a ROI and should be painted in the source image,
    add a contour_offset.
    """
    contour = contours[contour_id]

    M = cv.moments(contour)
    if M['m00'] == 0:
        print("Could not center by contour")
        return

    height, width = atlas_image.img.shape[:2]
    image_center = np.asarray((width, height), dtype=np.float32)/2.0

    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])
    c = (cx, cy) + contour_offset

    transform = np.eye(2, 3)
    transform[0,2] = (image_center[0] - c[0])
    transform[1,2] = (image_center[1] - c[1])
    atlas_image.transform_affine(transform)

################################################################

# ---------------
# Image Filtering
# ---------------

def feature_noisefilter_heatmap_canny(image, bin_size=25, filter_threshold_trigger=50, filter_threshold_invalid=35, canny_threshold1=100, canny_threshold2=200):
    """Convenience method calling canny before feature_noisefilter_heatmap."""

    img_edges = cv.Canny(image, threshold1=canny_threshold1, threshold2=canny_threshold2)
    return feature_noisefilter_heatmap(img_edges, bin_size, filter_threshold_trigger, filter_threshold_invalid)

def feature_noisefilter_components_canny(image, filter_threshold=50, canny_threshold1=100, canny_threshold2=200):
    """Convenience method calling canny before feature_noisefilter_components."""

    img_edges = cv.Canny(image, threshold1=canny_threshold1, threshold2=canny_threshold2)
    return feature_noisefilter_components(img_edges, filter_threshold, canny_threshold1, canny_threshold2)

def feature_noisefilter_heatmap(image_features, bin_size=25, filter_threshold_trigger=50, filter_threshold_invalid=35, roi_from=None, roi_to=None):
    """Filters noise from a feature image via a heatmap and returns both."""

    img_f = image_features
    height, width = img_f.shape

    heatmap = generate_heatmap(img_f, bin_size, roi_from, roi_to, resize=False, pad_output=False, apply_color=False)
    h_height, h_width = heatmap.shape

    # Filter border elements regardlessly
    for ey in range(0, height):
        # Filter from left
        for ix in range(0, bin_size + 1):
            img_f[ey][ix] = 0
        # Filter from right (including pixels which could not be binned)
        for ix in range((h_width - 1) * bin_size, width):
            img_f[ey][ix] = 0

    for ex in range(0, width):
        # Filter from top
        for iy in range(0, bin_size + 1):
            img_f[iy][ex] = 0
        # Filter from bottom (including pixels which could not be binned)
        for iy in range((h_height - 1) * bin_size, height):
            img_f[iy][ex] = 0

    # Locate and remove pixels/features within isolated heatmap features
    for hy in range(1, h_height - 1):
        for hx in range(1, h_width - 1):
            if heatmap[hy][hx] < filter_threshold_trigger:
                directions = np.zeros(8, dtype=np.bool_)

                # upper left
                # left
                # bottom left
                directions[0] = heatmap[hy + 1][hx - 1] < filter_threshold_invalid
                directions[1] = heatmap[hy + 0][hx - 1] < filter_threshold_invalid
                directions[2] = heatmap[hy - 1][hx - 1] < filter_threshold_invalid

                # upper mid
                # bottom mid
                directions[3] = heatmap[hy + 1][hx] < filter_threshold_invalid
                directions[4] = heatmap[hy - 1][hx] < filter_threshold_invalid

                # upper right
                # right
                # bottom right
                directions[5] = heatmap[hy + 1][hx + 1] < filter_threshold_invalid
                directions[6] = heatmap[hy + 0][hx + 1] < filter_threshold_invalid
                directions[7] = heatmap[hy - 1][hx + 1] < filter_threshold_invalid

                # The heatmap feature is surrounded by insignificant features or has max 2 significant neighbors
                if np.sum(directions) > 6:
                    start_y = hy * bin_size
                    start_x = hx * bin_size

                    heatmap[hy][hx] = 0

                    for ey in range(start_y, start_y + bin_size):
                        for ex in range(start_x, start_x + bin_size):
                            img_f[ey][ex] = 0

    return img_f, heatmap

def feature_noisefilter_components(image_features, filter_threshold=50, canny_threshold1=100, canny_threshold2=200):
    """Filters noise from a feature image via OpenCVs components and returns the filtered image."""

    img_f = image_features

    # Get edges to find features in image
    fsize = image_features.shape

    # Connectivity type 4 or 8
    connectivity = 4  
    
    output = cv.connectedComponentsWithStats(img_f, connectivity, cv.CV_32S)

    # Number of labels
    label_count = output[0]
    # Labels (matrix)
    labels = output[1]
    # Stats (matrix)
    stats = output[2]
    # Centroids (matrix)
    centroids = output[3]

    # Process each pixels label
    for y in range(0, fsize[0]):
        for x in range(0, fsize[1]):
            # Background label: nothing
            if labels[y][x] == 0:
                pass

            # Filter pixel, if its assigned component covers less area than threshold
            pixel_label = labels[y][x]
            if stats[pixel_label][cv.CC_STAT_AREA] < filter_threshold:
                img_f[y][x] = 0

    return img_f

def find_contour(atlas_image, anchor0=None, anchor1=None):
    """Returns an image containing contours based on features."""
    # Input image
    img = atlas_image.img
    height, width = img.shape[:2]

    # Processed ROI
    roi_ox = 0
    roi_oy = 0
    roi_height, roi_width = img.shape[:2]
    
    # If ROI: Crop image to fit the selected rectangle
    if (anchor0 is not None) and (anchor1 is not None):
        anchor_min, anchor_max = get_extreme_anchors(anchor0, anchor1)

        roi_ox = anchor_min[0]
        roi_oy = anchor_min[1]
        
        x_extent = anchor_max[0] - anchor_min[0]
        y_extent = anchor_max[1] - anchor_min[1]

        img = crop_image(img, roi_ox, roi_oy, x_extent, y_extent)
        
    # Find edge features and filter the results
    # 1) This method uses OpenCVs findcomponents method
    # img_edges = feature_noisefilter_components_canny(img, canny_threshold1=0, canny_threshold2=200)
    # heatmap = None
    
    # 2) This method is custom made and uses a feature heatmap
    bin_size=32
    img_edges, heatmap = feature_noisefilter_heatmap_canny(img, bin_size=bin_size)
    heatmap = fit_heatmap(heatmap, bin_size, width, height, roi_ox, roi_oy, True)

    # Dilate (enlarge) the edges such that gaps in the shapes are closed
    img_edges = dilate_contours(img_edges, kernel_size=9)

    # Finally find the most external contours
    contours, _ = cv.findContours(img_edges, mode=cv.RETR_EXTERNAL, method=cv.CHAIN_APPROX_SIMPLE)

    # Preprocess contour information
    image_center = np.array((width, height), dtype=np.float32)/2
    contour_areas = []
    contour_centers = []
    contour_cdistances = []
    min_cdistance = INFINITY
    best_contour_index = 0
    for i in range(0, len(contours)):
        con = contours[i]

        M = cv.moments(con)
        if M['m00'] == 0:
            contour_areas.append(None)
            contour_centers.append(None)
            contour_cdistances.append(None)
            continue

        # Contour area
        area = cv.contourArea(contours[i])
        contour_areas.append(area)

        # Contour center
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])
        c = (cx, cy)
        contour_centers.append(c)

        # Contour center squared distance (to image center)
        vec = image_center - c
        squared_length = np.dot(vec, vec)
        contour_cdistances.append(squared_length)

        # Detect the eye contour by taking the closest contour to center
        # TODO: Find better algorithm to detect the eye
        #       Though it works quite good atm
        if (squared_length < min_cdistance) or (area > 100 * contour_areas[best_contour_index]):
            min_cdistance = squared_length
            best_contour_index = i

    # Make Heatmap printable by OpenCV
    #if heatmap is not None:
    #    heatmap = cv.resize(heatmap, (target_size[1], target_size[0]), interpolation=cv.INTER_NEAREST_EXACT).astype(np.uint8)
    #    heatmap = cv.applyColorMap(heatmap, cv.COLORMAP_HOT)

    if True:
        # Remove all other image content based on best contour
        mask = np.zeros((height, width), dtype=np.uint8)
        mask = cv.drawContours(mask, contours, best_contour_index, color=(255,255,255), thickness=-1, offset=(roi_ox, roi_oy))
        atlas_image.filtered_img = cv.bitwise_and(img, img, mask=mask)
        # Correct background to be white
        if (BACKGROUND_WHITE):
            mask_inv = np.ones((height, width), dtype=np.uint8)
            mask_inv = cv.drawContours(mask_inv, contours, best_contour_index, color=(0,0,0), thickness=-1, offset=(roi_ox, roi_oy))
            atlas_image.filtered_img = cv.bitwise_not(atlas_image.filtered_img, atlas_image.filtered_img, mask=mask_inv)
        

        img_contours = np.zeros((height, width), dtype=np.uint8)
        img_contours = cv.drawContours(img_contours, contours, best_contour_index, color=(255,255,255), thickness=2, offset=(roi_ox, roi_oy))

        atlas_image.contour_points = contours[best_contour_index]
        
        # Draw contour centers
        #best_center = np.array(contour_centers[best_contour_index]) + np.array((roi_ox, roi_oy))
        #img_contours = cv.circle(img_contours, np.array(contour_centers[best_contour_index]) + np.array((roi_ox, roi_oy)), 4, (0, 0, 255), -1)

    else:
        # Draw all contours
        img_contours = np.zeros((height, width), dtype=np.uint8)
        img_contours = cv.drawContours(img_contours, contours, -1, color=(255,255,255), thickness=2, offset=(roi_ox, roi_oy))

        # Draw contour centers
        for center in contour_centers:
            if center is not None:
                cv.circle(img_contours, center + np.array((roi_ox, roi_oy)), 4, (0, 0, 255), -1)

    # Save
    #atlas_image.featuremap = img_edges
    #atlas_image.heatmap = heatmap
    #atlas_image.contourmap = img_contours

    best_center = contour_centers[best_contour_index]

    # TODO: Temporary
    # Save best centering transformation
    transform = np.eye(2, 3)
    transform[0,2] = (image_center[0] - best_center[0])
    transform[1,2] = (image_center[1] - best_center[1])
    atlas_image.transform_affine(transform)

# ----------------
# Image Processing
# ----------------

def generate_heatmap(img, bin_size:int=25, anchor0:tuple=None, anchor1:tuple=None, resize:bool=True, pad_output:bool=True, apply_color:bool=True):
    """Generates a heatmap based on some image input.
    
    Parameters
    ----------
    img : Image
        Images used for processing
    bin_size : Integer
        Symmetric region summed up into a heatmap pixel
    anchor0 : Tuple(int)
        Lower left corner of ROI
    anchor1 : Tuple(int)
        Upper right corner of ROI
    resize : Boolean
        Resize heatmap bin the bin size (contains only binned pixels)
    pad_output : Boolean
        Pads heatmap such that it matches the input image size
        Only works if resize is also true.
    apply_color : Boolean
        Transforms heatmap into BGR-image with color mapping
    """
    # Convert to intensity if needed
    if len(img.shape) > 2:
        img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    # Region of the image which is converted into a heatmap
    roi_ox = 0  # Offset to processed image (ROI)
    roi_oy = 0
    roi_height, roi_width = img.shape       # Shape of processed image (ROI)
    target_height, target_width = img.shape # Shape of input image

    # Crop image if a smaller ROI has been selected
    if (anchor0 is not None) and (anchor1 is not None):
        anchor_min, anchor_max = get_extreme_anchors(anchor0, anchor1)
        
        # Make sure ROI has at least size of bin_size (else some dimensions have 0 extents)
        if (anchor_max[0] - anchor_min[0] >= bin_size) and (anchor_max[1] - anchor_min[1] >= bin_size):
            roi_ox = anchor_min[0]
            roi_oy = anchor_min[1]
            
            roi_width = anchor_max[0] - anchor_min[0]
            roi_height = anchor_max[1] - anchor_min[1]

            img = crop_image(img, roi_ox, roi_oy, roi_width, roi_height)

    # Kernel: average pixel-values (255 for presence of edge) within window
    kernel = np.ones((bin_size, bin_size), np.float32) / 255
    # Heatmap: dimension reduced image
    heatmap = strided_convolution(img, kernel, bin_size)
    h_height, h_width = heatmap.shape

    # Resize heatmap (excluding area of ignored pixels) for display in opencv
    if resize:
        heatmap = cv.resize(heatmap, (h_width * bin_size, h_height * bin_size), interpolation=cv.INTER_NEAREST_EXACT).astype(np.uint8)
    
    # If enabled, pad heatmap such that it matches input image size
    if pad_output and resize:
        heatmap = pad_image(heatmap, target_width, target_height, roi_ox, roi_oy)

    if apply_color:
        heatmap = cv.applyColorMap(heatmap, cv.COLORMAP_HOT)

    return heatmap

def extract_outer_contours(featuremap):
    """Extracts the outermost contours.
    
    Input should be an edgemap (e.g. preprocess with canny).
    For better results dilate the features (edges).
    Returns contours & id of most significant image element.
    """
    fmap = featuremap

    # Input should be a luminance-only 2D featuremap
    if len(fmap.shape) != 2:
        return None

    # Input image
    height, width = fmap.shape[:2]

    # Find the most external contours
    contours, _ = cv.findContours(fmap, mode=cv.RETR_EXTERNAL, method=cv.CHAIN_APPROX_SIMPLE)

    # Find best contour
    # Preprocess..
    fmap_center = np.array((width, height), dtype=np.float32)/2
    contour_areas = []
    contour_centers = []
    contour_distances = []    # distance to center
    min_distance = INFINITY   # distance to center
    best_contour_id = 0
    for i in range(0, len(contours)):
        con = contours[i]

        M = cv.moments(con)
        if M['m00'] == 0:
            contour_areas.append(None)
            contour_centers.append(None)
            contour_distances.append(None)
            continue

        # Contour area
        area = cv.contourArea(contours[i])
        contour_areas.append(area)

        # Contour center
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])
        c = (cx, cy)
        contour_centers.append(c)

        # Contour center squared distance (to image center)
        vec = fmap_center - c
        squared_length = np.dot(vec, vec)
        contour_distances.append(squared_length)

        # Detect the eye contour by taking the closest contour to center
        # TODO: Find better algorithm to detect the eye
        #       Though it works quite good atm
        if (squared_length < min_distance) or (area > 100 * contour_areas[best_contour_id]):
            min_distance = squared_length
            best_contour_id = i

    return (contours, best_contour_id)

def draw_contour(image_shape, contours, contour_id, contour_offset=(0,0), color=(255,255,255), thickness=2):
    """Draws the image contours on a black image.

    If contours were gathered in a ROI and should be painted in the source image,
    add a contour_offset.
    If the contours should be drawn as filled rather than outlines, set thickness=-1.
    """
    img_contours = np.zeros(image_shape, dtype=np.uint8)
    img_contours = cv.drawContours(img_contours, contours, contour_id, color=color, thickness=thickness, offset=contour_offset)

    return img_contours

def filter_by_mask(image, mask):
    """Filters all pixels outside mask to black.

    Mask and image must have same shape.
    """
    if (len(image.shape[:2]) != len(mask.shape[:2])):
        return None

    masked_img = cv.bitwise_and(image, image, mask=mask)
    # Correct background color
    if (BACKGROUND_WHITE):
        mask_inv = np.ones(image.shape[:2], dtype=np.uint8)
        mask_inv = cv.drawContours(mask_inv, contours, contour_id, color=(0,0,0), thickness=-1, offset=contour_offset)
        masked_img = cv.bitwise_not(masked_img, masked_img, mask=mask_inv)

    return masked_img

def filter_by_contour(image, contours, contour_id, contour_offset=(0,0)):
    """Filters all pixels outside contour to black.

    If contours were gathered in a ROI and should be painted in the source image,
    add a contour_offset.
    """

    mask = np.zeros(image.shape[:2], dtype=np.uint8)
    mask = cv.drawContours(mask, contours, contour_id, color=(255,255,255), thickness=-1, offset=contour_offset)
    masked_img = cv.bitwise_and(image, image, mask=mask)
    # Correct background color
    if (BACKGROUND_WHITE):
        mask_inv = np.ones(image.shape[:2], dtype=np.uint8)
        mask_inv = cv.drawContours(mask_inv, contours, contour_id, color=(0,0,0), thickness=-1, offset=contour_offset)
        masked_img = cv.bitwise_not(masked_img, masked_img, mask=mask_inv)

    return masked_img

################################################################

# ----------
# Data Types
# ----------

class Window:
    """A window with a renderable canvas."""

    name = None
    canvas = None
    overlay = None

    

    def __init__(self, window_name: str, shape:tuple=(768,1024)):
        self.name = window_name

        cv.namedWindow(self.name, cv.WINDOW_NORMAL)
        self.canvas = np.zeros(shape)
        self.clear()

    def update(self, image: np.ndarray):
        self.canvas = copy.copy(image)

    def update_overlay(self, image: np.ndarray):
        self.overlay = copy.copy(image)

    def clear(self):
        np.ndarray.fill(self.canvas, 240)

    def clear_overlay(self):
        self.overlay = None

    def render(self):
        if self.overlay is not None:
            cv.imshow(self.name, self.overlay)
        else:
            cv.imshow(self.name, self.canvas)

class Controls:
    """Holds all control data."""

    key_dict = {
        "quit": (27, "ESC", "Quit"),
        "discard": (ord("d"), "D", "Discard Image"),
        "undo": (ord("z"), "Z", "Undo"),
        "align": (ord("a"), "A", "Align"),
        "save": (ord("s"), "S", "Save"),
        "mode_1": (ord("1"), "1", "Mode: Show Image"),
        "mode_2": (ord("2"), "2", "Mode: Show Filtered Image"),
    }

    def get_keymap(self):
        return self.key_dict

    def get_keymap_image(self, resolution: tuple):
        assert len(resolution) == 2

        # Create keymap text
        keymap_text = "\n"
        for elem in self.key_dict:
            keymap_text += f"{self.key_dict[elem][1]} - {self.key_dict[elem][2]}\n"
        
        # Draw keymap text
        img = np.zeros((resolution[0], resolution[1], 3), np.uint8)
        np.ndarray.fill(img, 240)

        font_color = (0, 0, 0)
        font_scale = 0.8
        font_thickness = 1
        outline_color = (0, 0, 0)
        line_spacing = 1.5
        cv_draw_multiline_text(img, keymap_text, (0, 0), font_color, font_scale, font_thickness, cv.FONT_HERSHEY_PLAIN, None, line_spacing, cv.LINE_AA)

        return img

class Image:
    """Container for image data and their respective transformations.
    
    Attributes
    ----------
    img : np.ndarray
        Image pixel data
    score : double
        Alignment rating
    """
    name = None
    img = None
    featuremap = None
    heatmap = None
    contourmap = None
    filtered_img = None
    idx = None
    
    extents = None
    score = None
    scale_x = 1.0
    shear_xy = 0.0
    scale_y = 1.0
    shear_yx = 0.0
    contour_points = None

    discard = False
    lock = None

    transforms = []

    def __init__(self, img, idx, name:str = None, score_initial:float = 1.0):
        self.name = name
        self.img = img
        self.idx = idx
        #self.featuremap = np.zeros_like(img)
        #self.heatmap = np.zeros_like(img)
        #self.contourmap = np.zeros_like(img)
        self.filtered_img = np.zeros_like(img)
        self.extents = img.shape[:2]

        self.score = score_initial
        self.lock = threading.Lock()

    def transform_affine(self, transform, flags=cv.INTER_LANCZOS4):
        while self.lock.locked():
            pass

        self.lock.acquire()

        self.transforms.append(transform)
        self.img = cv.warpAffine(self.img, transform, (self.img.shape[1], self.img.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)
        if self.featuremap is not None:
            self.featuremap = cv.warpAffine(self.featuremap, transform, (self.featuremap.shape[1], self.featuremap.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)
        if self.heatmap is not None:
            self.heatmap = cv.warpAffine(self.heatmap, transform, (self.heatmap.shape[1], self.heatmap.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)
        if self.contourmap is not None:
            self.contourmap = cv.warpAffine(self.contourmap, transform, (self.contourmap.shape[1], self.contourmap.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)
        self.filtered_img = cv.warpAffine(self.filtered_img, transform, (self.filtered_img.shape[1], self.filtered_img.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)

        self.lock.release()


    def transform_perspective(self, transform, flags=cv.INTER_LANCZOS4):
        while self.lock.locked():
            pass

        self.lock.acquire()

        self.transforms.append(transform)
        self.img = cv.warpPerspective(self.img, transform, (self.img.shape[1], self.img.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)
        if self.featuremap is not None:
            self.featuremap = cv.warpPerspective(self.featuremap, transform, (self.featuremap.shape[1], self.featuremap.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)
        if self.heatmap is not None:
            self.heatmap = cv.warpPerspective(self.heatmap, transform, (self.heatmap.shape[1], self.heatmap.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)
        if self.contourmap is not None:
            self.contourmap = cv.warpPerspective(self.contourmap, transform, (self.contourmap.shape[1], self.contourmap.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)
        self.filtered_img = cv.warpPerspective(self.filtered_img, transform, (self.filtered_img.shape[1], self.filtered_img.shape[0]), borderMode=cv.BORDER_REPLICATE, flags=flags)

        self.lock.release()

    def get_by_mode(self, mode:int):
        if self.lock.locked():
            return np.zeros(self.extents, dtype=np.uint8)

        if mode == 1:
            return self.img

        if mode == 2:
            if self.filtered_img is not None:
                return self.filtered_img

        return self.img

    def set_by_mode(self, mode, image):
        while self.lock.locked():
            pass

        self.lock.acquire()

        if mode == 1:
            self.img = image
        
        if mode == 2:
            self.filtered_img = image

        self.lock.release()

    def available(self):
        return not self.lock.locked()

class ImageAtlas:
    """Class to hold an manage loaded images.
    
    Can be indexed directly to access only active images.
    Keeps track of discarded images and can recover them.
    Holds for each active image a transformation matrix and correlation coefficient.

    Attributes
    ----------
    images_active : Image
        Images used for processing
    images_discarded : Image
        Images not used for processing (can be recovered)
    """
    images_active = []
    images_discarded = []

    def __getitem__(self, key):
        return self.images_active[key]

    def __setitem__(self, key, value):
        self.images_active[key] = value

    def __len__(self):
        return len(self.images_active)

    def load_images(self, path:str, flags:int=cv.IMREAD_COLOR):
        if not os.path.exists(path):
            print("Does not exist: " + path)
            return

        if os.path.isfile(path):
            self.__load_image_from_file(path, flags)
        elif os.path.isdir(path):
            self.__load_images_from_folder(path, flags)
        else:
            print("Could not figure out path: " + path)

    def __load_images_from_folder(self, path:str, flags:int=cv.IMREAD_COLOR):
        for filename in os.listdir(path):
            if os.path.isdir(path + os.path.sep + filename):
                continue
            self.__load_image_from_file(os.path.join(path, filename), filename, flags)

    def __load_image_from_file(self, path:str, filename:str, flags:int=cv.IMREAD_COLOR):
        #img = cv.imread(path)
        #if img is not None:
        #    if np.any(np.array(img.shape) > 2000):
        #        scale_down = 0.5
        #        img = cv.resize(img, None, fx= scale_down, fy= scale_down, interpolation= cv.INTER_LINEAR) 
        #    self.images_active.append(Image(img, name=filename))
        stream = open(path, "rb")
        bytes = bytearray(stream.read())
        numpyarray = np.asarray(bytes, dtype=np.uint8)
        img = cv.imdecode(numpyarray, cv.IMREAD_UNCHANGED)
        if img is not None:
            if np.any(np.array(img.shape) > 2000):
                scale_down = 0.5
                img = cv.resize(img, None, fx= scale_down, fy= scale_down, interpolation= cv.INTER_LINEAR) 
            self.images_active.append(Image(img, len(self.images_active), name=filename))

    def save_images(self, save_path:str="", dummies:bool = False, mode:int = 0):
        path = save_path
        
        if not path:
            path = f"out/mode{mode}/"
        elif path[-1] == "/":
            path = path + f"mode{mode}/"
        else:
            path = path + f"/mode{mode}/"

        Path(path).mkdir(parents=True, exist_ok=True)
        

        if dummies:
            for discarded in self.images_discarded:
                replicate_img = None
                for img in self.images_active:
                    if discarded[0].idx > img.idx:
                        replicate_img = img
                
                if replicate_img is None:
                    if len(self.images_active) > 0:
                        replicate_img = self.images_active[0]
                            
                if replicate_img is None:
                    continue
                
                filepath = path + discarded[0].name
                #cv.imwrite(filepath, dummy_img)
                buf = cv.imencode("." + discarded[0].name.split(".")[-1], replicate_img.get_by_mode(mode))[1]
                stream = open(filepath, "wb")
                stream.write(buf)
                stream.close()

                
                

        for img in self.images_active:
            filepath = path + img.name
            #cv.imwrite(filepath, img.get_by_mode(mode))
            buf = cv.imencode("." + img.name.split(".")[-1], img.get_by_mode(mode))[1]
            stream = open(filepath, "wb")
            stream.write(buf)
            stream.close()

    def clear(self):
        self.images_active.clear()
        self.images_discarded.clear()

    def mark_discard(self, pos: int):
        if len(self.images_active) > 0:
            self.images_active[pos].discard = True

    def process_discard(self):
        i = 0

        while(i < len(self.images_active)):
            if self.images_active[i].discard:
                self.images_discarded.append((self.images_active[i], i))
                self.images_active.pop(i)
            else:
                i += 1

    def undo_discard(self):
        if len(self.images_discarded) > 0:
            discarded_img = self.images_discarded[-1]
            discarded_img[0].discard = False
            self.images_discarded.pop()
            self.images_active.insert(discarded_img[1], discarded_img[0])
            return discarded_img[1]

    def get_num_active(self):
        return len(self.images_active)

    def get_num_discarded(self):
        return len(self.images_discarded)

class StateInfo:
    """Container to hold application specific information to be displayed or accessed."""

    num_images_loaded = 0
    num_images_working = 0
    num_images_discarded = 0

    current_score = 0

    time_per_update = 0
    time_per_render = 0

    mouse_x = 0
    mouse_y = 0
    mouse_lbutton = False
    mouse_rbutton = False
    mouse_lbutton_drag = False
    mouse_rbutton_drag = False
    anchor0_x = 0
    anchor0_y = 0
    anchor1_x = 0
    anchor1_y = 0
    draw_gui = False

    def get_stateinfo_image(self, resolution: tuple, scale: tuple, shear: tuple, thread1, thread2):
        if len(resolution) > 2:
            resolution = resolution[:2]

        # Create stateinfo text
        stateinfo_text = f"\n"
        stateinfo_text += f"Images loaded: {self.num_images_loaded}\n"
        stateinfo_text += f"Images kept: {self.num_images_working}\n"
        stateinfo_text += f"Images discarded: {self.num_images_discarded}\n\n"
        #stateinfo_text += f"Ms per update: {self.time_per_update}\n"
        #stateinfo_text += f"Ms per render: {self.time_per_render}\n\n"

        stateinfo_text += f"Image Thread 1 (asc): {thread1}\n"
        stateinfo_text += f"Image Thread 2 (desc): {thread2}\n"
        progress = (thread_a_percent + thread_d_percent) / 2.0
        stateinfo_text += f"Overall process: {progress:.2f}%\n\n"

        #stateinfo_text += f"Mouse (x, y): {(self.mouse_x, self.mouse_y)}\n"
        #stateinfo_text += f"Anchor0 (x, y): {(self.anchor0_x, self.anchor0_y)}\n"
        #stateinfo_text += f"Anchor1 (x, y): {(self.anchor1_x, self.anchor1_y)}\n"
        #stateinfo_text += f"Left Click: {self.mouse_lbutton}\n"
        #stateinfo_text += f"Left Drag: {self.mouse_lbutton_drag}\n"
        #stateinfo_text += f"Right Click: {self.mouse_rbutton}\n"
        #stateinfo_text += f"Right Drag: {self.mouse_rbutton_drag}\n\n"

        stateinfo_text += f"Current Score: {self.current_score:.4f}\n"
        stateinfo_text += f"Scale X (ca): {scale[0]:.4f}\n"
        #stateinfo_text += f"Shear XY (ca): {shear[0]:.4f}\n"
        stateinfo_text += f"Scale Y (ca): {scale[1]:.4f}\n"
        #stateinfo_text += f"Shear YX (ca): {shear[1]:.4f}\n\n"


        # Draw stateinfo text
        img = np.zeros((resolution[0], resolution[1], 3), np.uint8)
        np.ndarray.fill(img, 240)

        font_color = (0, 0, 0)
        font_scale = 0.8
        font_thickness = 1
        outline_color = (0, 0, 0)
        line_spacing = 1.5
        cv_draw_multiline_text(img, stateinfo_text, (0, 0), font_color, font_scale, font_thickness, cv.FONT_HERSHEY_PLAIN, None, line_spacing, cv.LINE_AA)

        return img
    
    def update_mouse(self, pos_x, pos_y, lpressed, rpressed):
        self.mouse_leftpressed = lpressed
        self.mouse_rightpressed = rpressed
        self.mouse_x = pos_x
        self.mouse_y = pos_y

################################################################

# -----------
# Application
# -----------

#def initialize():
    if (no_arguments):
        # Change working directory to script location
        script_path = os.path.abspath(__file__)
        dir_name = os.path.dirname(script_path)
        os.chdir(dir_name)

def nothing(x):
    pass

def main():
    global thread_asc
    global thread_desc
    global quality_threshold
    global thread_a_percent
    global thread_d_percent
    
    #initialize()
    atlas = ImageAtlas()
    atlas.load_images(data_location)

    control = Controls()
    control_keys = control.get_keymap()

    align_proc = False

    # Image Window
    ##############
    image_window = Window("image", (300, 512))
    img_nr = 0
    img_nr_prev = -1
    cv.createTrackbar("Show Image", "image", 0, len(atlas) - 1, nothing)

    # Menu Window
    #############
    menu_shape = (300, 200)
    menu_window = Window("menu", menu_shape)
    menu_window.update(control.get_keymap_image(menu_shape))

    # Info Window
    #############
    info_shape = (300, 200)
    info_window = Window("info", info_shape)

    stateinfo = StateInfo()
    stateinfo.num_images_loaded = len(atlas)

    for i in range(0, len(atlas)):
        preprocess_image(atlas[i],
        bin_size=32,
        hm_threshold1=50,
        hm_threshold2=35,
        canny_threshold1=100,
        canny_threshold2=200,
        dilation_kernel=9
    )

    start_update = 0
    end_update = 0
    end_render = 0
    mode = 0

    # Render app
    while(True):
        start_update = time.time()

        # Update callback
        #cv.setMouseCallback(image_window.name, mouse_cb, (atlas, img_nr, stateinfo))

        # Fetch selected area from GUI
        #anchor0, anchor1 = gui_get_selected_area(stateinfo)

        # Update input
        k = cv.waitKey(1)
        if k == control_keys["quit"][0]:
            os._exit(1)
        
        elif k == control_keys["discard"][0]:
            if len(atlas) > 0:
                atlas.mark_discard(img_nr)
                atlas.process_discard()
                img_nr = min(img_nr, len(atlas) - 1)
                img_nr = max(img_nr, 0)
                cv.setTrackbarMax("Show Image", "image", len(atlas) - 1)
                cv.setTrackbarPos("Show Image", "image", img_nr)

        elif k == control_keys["undo"][0]:
            img_nr = atlas.undo_discard()
            cv.setTrackbarPos("Show Image", "image", img_nr)

        elif k == control_keys["align"][0]:
            thread_a_percent = 0.0
            thread_d_percent = 0.0
            if thread_asc.is_alive() or thread_desc.is_alive():
                print("Threads are already running")
                pass
            else:
                thread_asc = threading.Thread(target=threaded_align_ECC, args=(img_nr, len(atlas) - 1, atlas, mode, quality_threshold))
                thread_desc = threading.Thread(target=threaded_align_ECC, args=(img_nr, 0, atlas, mode, quality_threshold))
                thread_asc.start()
                thread_desc.start()
                #thread_asc.join()
                #thread_desc.join()

        elif k == control_keys["save"][0]:
            if len(atlas) > 0:
                atlas.save_images(save_path=out_location,dummies=save_placeholders, mode=mode)

        elif k == control_keys["mode_1"][0]:
            mode = 1

        elif k == control_keys["mode_2"][0]:
            mode = 2
            
        # Cleanup images marked for discard
        atlas.process_discard()

        # Update GUI
        img_nr = cv.getTrackbarPos("Show Image", image_window.name)
        cv.setTrackbarMax("Show Image", image_window.name, max(0, len(atlas) - 1))

        stateinfo.num_images_working = atlas.get_num_active()
        stateinfo.num_images_discarded = atlas.get_num_discarded()
        stateinfo.current_score = atlas[img_nr].score

        # Toggle overlaid image off if another image is chosen
        if img_nr != img_nr_prev:
            image_window.clear_overlay()


        # Render
        if len(atlas) > 0:
            image_window.update(atlas[img_nr].get_by_mode(mode))
        else:
            image_window.clear()

        gui_update(image_window, stateinfo)


        info_window.update(stateinfo.get_stateinfo_image(info_window.canvas.shape,
            (atlas[img_nr].scale_x, atlas[img_nr].scale_y),
            (atlas[img_nr].shear_xy, atlas[img_nr].shear_yx),
            thread_a_prog,
            thread_d_prog
        ))

        end_update = time.time()

        image_window.render()
        menu_window.render()
        info_window.render()

        end_render = time.time()

        stateinfo.time_per_update = end_update - start_update
        stateinfo.time_per_render = end_render - end_update

        img_nr_prev = img_nr

################################################################

# ----
# Entry
# ----

main()

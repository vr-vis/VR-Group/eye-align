from ast import Delete
import os
import cv2 as cv
import numpy as np

# ------
# Global
# ------

no_arguments = True
skip_black_imgs = True
folder = "rabbit2"
mode = 4

fps = 20


# -------
# General
# -------

def initialize():
    if (no_arguments):
        # Change working directory to script location
        script_path = os.path.abspath(__file__)
        dir_name = os.path.dirname(script_path)
        os.chdir(dir_name)

def test_black(frame):
    f_gray = frame
    if len(f_gray.shape) > 2:
        f_gray = cv.cvtColor(f_gray, cv.COLOR_BGR2GRAY)

    if cv.countNonZero(f_gray) == 0:
        return True
    
    return False

def shrink_frame(frame, max_size):
    scale_down = 0.5
    if (np.any(np.asarray(frame.shape[:2]) > max_size)):
        frame = cv.resize(frame, None, fx= scale_down, fy= scale_down, interpolation= cv.INTER_LINEAR)
    
    return frame

# ----
# Main
# ----

def main():
    initialize()

    image_folder = f"out/{folder}/mode{mode}"
    video_name = f"{folder}_mode{mode}.mp4v"

    #image_folder = f"../Data/{folder}"
    #video_name = f"{folder}_src.mp4v"

    if not os.path.isdir(image_folder):
        print(f"{image_folder} is not a directory")
        return

    images = [img for img in os.listdir(image_folder)]
    if len(images) < 1:
        print(f"{image_folder} is empty")
        return

    frame = cv.imread(os.path.join(image_folder, images[0]))
    if frame is None:
        print(f"First frame corrupted from: {image_folder}")
        return


    # Initialize video
    frame = shrink_frame(frame, max_size=2000)
    height, width = frame.shape[:2]

    is_color = len(frame.shape) > 2
    fourcc = cv.VideoWriter_fourcc(*'mp4v')
    video = cv.VideoWriter(video_name, fourcc, fps, (width,height), is_color)

    # Start capturing
    for img in images:
        path = os.path.join(image_folder, img)
        frame = cv.imread(path)

        if frame is None:
            continue

        if skip_black_imgs and test_black(frame):
            continue

        frame = shrink_frame(frame, max_size=2000)
        
        video.write(frame)

    cv.destroyAllWindows()
    video.release()

# ----
# Entry
# ----

main()

print("Done")
